const inputs = document.querySelectorAll('input');
const inputHour = document.querySelector('input.hour');
const inputMinute = document.querySelector('input.minute');
const inputSecond = document.querySelector('input.second');
const start = document.querySelector('.start');
const reset = document.querySelector('.reset');

let fullTime = 0;
let passedSeconds = 1;
let interval;

const flip = (container, number) => {
  const currentNumber = container.dataset.number;
  const baseTop = container.querySelector('.base .top');
  const baseBottom = container.querySelector('.base .bottom');
  const flaps = container.querySelectorAll('.flap');
  const front = container.querySelector('.front');
  const back = container.querySelector('.back');

  container.dataset.number = number;
  front.dataset.content = currentNumber;
  back.dataset.content = number;

  baseTop.innerHTML = number;

  flaps.forEach((flap) => {flap.classList.add('show');});

  setTimeout(() => {
    flaps.forEach((flap) => {flap.classList.remove('show');});
    baseBottom.innerHTML = number;
  }, 600);
};

const set = (container, number) => {
  const base = container.querySelectorAll('.base div');

  container.dataset.number = number;
  base.forEach((div) => {div.innerHTML = number;});
};

const update = (key, time, shouldFlip) => {
  let currentTime = String(time);

  const container1 = document.querySelector(`.${key}-1`);
  const container2 = document.querySelector(`.${key}-2`);

  if (currentTime.length === 1) currentTime = `0${time}`;

  if (Number(currentTime) < 0) currentTime = '00';
  if (Number(currentTime) > 99) currentTime = '99';
  if (!Number.isInteger(Number(currentTime))) currentTime = '00';

  const number1 = Number(currentTime.substr(0, 1));
  const number2 = Number(currentTime.substr(1, 1));

  if (Number(container1.dataset.number) !== number1) {
    shouldFlip
      ? flip(container1, number1)
      : set(container1, number1);
  }

  if (Number(container2.dataset.number) !== number2) {
    shouldFlip
      ? flip(container2, number2)
      : set(container2, number2);
  }
};

const setTimer = (totalSeconds) => {
  const hours = Math.floor(totalSeconds / 60 / 60);
  const minutes = Math.floor((totalSeconds % (60 * 60)) / 60);
  const seconds = totalSeconds % 60;

  update('hour', hours, true);
  update('minute', minutes, true);
  update('second', seconds, true);
};

const setFullTime = () => {
  fullTime = Number(inputHour.value) * 60 * 60
  + Number(inputMinute.value) * 60
  + Number(inputSecond.value);
};

const startInterval = () => {
  interval = setInterval(() => {
    if (fullTime === passedSeconds) {
      reset.disabled = false;
      start.innerHTML = 'start';
      clearInterval(interval);
    }

    setTimer(fullTime - passedSeconds);
    passedSeconds += 1;
  }, 1000);
};

inputs.forEach((input) => {
  input.addEventListener('input', () => update(input.dataset.type, input.value, false));
});

start.addEventListener('click', () => {
  if (start.innerHTML === 'start') {
    reset.disabled = true;
    start.innerHTML = 'pause';
    setFullTime();
    startInterval();
  } else {
    reset.disabled = false;
    start.innerHTML = 'start';
    clearInterval(interval);
  }
});

reset.addEventListener('click', () => {
  clearInterval(interval);
  passedSeconds = 1;
  inputs.forEach((input) => {update(input.dataset.type, input.value, false);});
});
